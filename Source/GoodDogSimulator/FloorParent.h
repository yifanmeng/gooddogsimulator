// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FloorParent.generated.h"

UCLASS()
class GOODDOGSIMULATOR_API AFloorParent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloorParent();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere, Category = Floor)
		TSubclassOf<class AFloor> FloorClass;
	AFloor *TempFloor;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void CreateNewFloor();
	void SetSpawnTrue() { spawn = true; }
	int GetNumberOfObstacles();
	float GetSpeedLevel() { return speedLevel; }
	void IncreaseObstacles();
	void NextLevel();
	int GetConfiguration();
	//void IncreaseObstacles();

private:
	int floorsSpawned = 0;
	int obstacles;
	float speedLevel = 1;
	float speedUpInt;
	float interval;
	float addToInterval = 35;
	TArray<AFloor*> FloorArray;
	bool spawn;
};
