// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "DoggoGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GOODDOGSIMULATOR_API ADoggoGameMode : public AGameModeBase
{
	GENERATED_BODY()


	UFUNCTION(BlueprintCallable)
		void SetSaveGame(USaveGame* SG) { SaveGame = SG; };

	UFUNCTION(BlueprintCallable)
		USaveGame* GetSaveGame() { return SaveGame; };
	
private:
	USaveGame* SaveGame;
};
