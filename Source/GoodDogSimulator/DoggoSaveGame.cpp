// Fill out your copyright notice in the Description page of Project Settings.

#include "GoodDogSimulator.h"
#include "DoggoSaveGame.h"

UDoggoSaveGame::UDoggoSaveGame()
{
	names.Add("alex");
	names.Add("richard");
	names.Add("poop");

	scores.Add(FMath::RandRange(1000, 2000));
	scores.Add(0);
	scores.Add(500);
}

void UDoggoSaveGame::AddScore(FString name, FString score)
{
	scores.Add(FCString::Atoi(*score));
	names.Add(name);
	SortScores();
}

void UDoggoSaveGame::SortScores()
{
	for (int i = 0; i < scores.Num(); i++)
	{
		for (int j = 0; j < scores.Num(); j++)
		{
			if (scores[j] < scores[i]) 
			{
				int temp = scores[j];
				scores[j] = scores[i];
				scores[i] = temp;

				FString temps = names[j];
				names[j] = names[i];
				names[i] = temps;
			}
		}
	}
}

FString UDoggoSaveGame::GetScores()
{
	FString name = "";
	for (int i = 0; i < scores.Num(); i++)
	{
		name += names[i];
		name += ": ";
		name.AppendInt(scores[i]);
		name += "\n";
	}
	return name;
}
