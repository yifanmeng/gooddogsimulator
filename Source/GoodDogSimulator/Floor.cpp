// Fill out your copyright notice in the Description page of Project Settings.

//#include <stdlib.h>

#include "GoodDogSimulator.h"
#include "Math.h"
#include "FloorParent.h"
#include "Floor.h"


// Sets default values
AFloor::AFloor()
{
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	speed = 1000.0f;
	Y_Offset_Ball += FMath::RandRange(-300, 300);
	Z_Offset_Ball += FMath::RandRange(100, 150);
	Y_Offset_Bone += FMath::RandRange(-300, 300);
	Z_Offset_Bone += (FMath::RandRange(0, 1) == 0) ? 80 : 150;
	if (Cast<AFloorParent>(owner))
		config = Cast<AFloorParent>(owner)->GetConfiguration();

}


// Called when the game starts or when spawned
void AFloor::BeginPlay()
{
	Super::BeginPlay();
	SpawnBall();
	SpawnObstacle();
	SpawnBone();
	SpawnTable();
	SpawnTrees();
}

void AFloor::SpawnTrees() {
	for (int i = 0; i <= 3000; i+= 1000) {
		if (spawnTree != nullptr && i != 0) {
			FVector location = this->GetActorLocation();
			FRotator rotation = this->GetActorRotation();
			auto tree = GetWorld()->SpawnActor<AActor>(spawnTree, location, rotation);
			tree->SetActorLocation(location);
			trees.Add(tree);
			Y_Offsets_Trees.Add(i);
		}
	}
	for (int i = -4000; i < -1000; i += 1000) {
		if (spawnTree != nullptr && i != 0) {
			FVector location = this->GetActorLocation();
			FRotator rotation = this->GetActorRotation();
			rotation = rotation.GetInverse();
			auto tree = GetWorld()->SpawnActor<AActor>(spawnTree, location, rotation);
			tree->SetActorLocation(location);
			trees.Add(tree);
			Y_Offsets_Trees.Add(i);
		}
	}
}

void AFloor::SpawnTable() {
	if (FMath::RandRange(0, 4) == 4) {
		Y_Offset_Table = FMath::RandRange(-100, 100);
		FVector location = this->GetActorLocation();
		FRotator rotation = this->GetActorRotation();
		if (spawnTable != nullptr) {
			table = GetWorld()->SpawnActor<AActor>(spawnTable, location, rotation);
			table->SetActorLocation(location);
		}
	}
}

void AFloor::SpawnBall() {
	int number = FMath::RandRange(0, 100);
	if (number >= 95) {
		FVector location = this->GetActorLocation();
		FRotator rotation = this->GetActorRotation();
		if (spawnBall != nullptr) {
			ball = GetWorld()->SpawnActor<AActor>(spawnBall, location, rotation);
			ball->SetActorLocation(location);
		}
		CanSpawnBone = false;
	}
}

void AFloor::SpawnObstacle() {
	int num = FMath::RandRange(1, 7);
	if (num == 1) {
		SpawnInVerticalLine();
	}
	if (num == 2) {
		SpawnInHorizontalLine();
	}
	if (num == 3) {
		SpawnInDiagonal();
	}
	if (num == 4) {
		SpawnInVerticalLineOffset();
	}
	if (num == 5) {
		SpawnInHorizontalLineOffset();
	}
	if (num == 6) {
		SpawnOffCenter();
	}
	if (num == 7) {
		SpawnInGrid();
	}
}

void AFloor::SpawnInHorizontalLine() {
	int distance = FMath::RandRange(200, 350);
	int ndistance = distance *= -1;
	int offset = 0;
	FVector loc;
	FVector location = this->GetActorLocation();
	FRotator rotation = this->GetActorRotation();
	AActor * obst1 = GetWorld()->SpawnActor<AActor>(obstacle, location, rotation);
	X_Offsets.Add(0);
	Y_Offsets.Add(0);
	obstacles.Add(obst1);


	loc = location;
	loc.Y += distance;
	X_Offsets.Add(offset);
	Y_Offsets.Add(distance);
	AActor * obst5 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst5);

}

void AFloor::SpawnInVerticalLine() {
	int distance = FMath::RandRange(200, 350);
	int ndistance = distance *= -1;
	int offset = 0;
	FVector location = this->GetActorLocation();
	FVector loc;
	FRotator rotation = this->GetActorRotation();
	AActor * obst1 = GetWorld()->SpawnActor<AActor>(obstacle, location, rotation);
	X_Offsets.Add(0);
	Y_Offsets.Add(offset);
	obstacles.Add(obst1);

	loc = location;
	loc.X += distance;
	X_Offsets.Add(distance);
	Y_Offsets.Add(offset);
	AActor * obst5 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst5);
}

void AFloor::SpawnInHorizontalLineOffset() {
	int distance = FMath::RandRange(200, 350);
	int ndistance = distance *= -1;
	int offset = FMath::RandRange(-300, 350);
	FVector loc;
	FVector location = this->GetActorLocation();
	FRotator rotation = this->GetActorRotation();
	AActor * obst1 = GetWorld()->SpawnActor<AActor>(obstacle, location, rotation);
	X_Offsets.Add(offset);
	Y_Offsets.Add(0);
	obstacles.Add(obst1);

	loc = location;
	loc.Y += distance;
	X_Offsets.Add(offset);
	Y_Offsets.Add(distance);
	AActor * obst5 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst5);

}

void AFloor::SpawnInVerticalLineOffset() {
	int distance = FMath::RandRange(200, 750);
	int ndistance = distance *= -1;
	int offset = FMath::RandRange(-300, 350);
	FVector location = this->GetActorLocation();
	FVector loc;
	FRotator rotation = this->GetActorRotation();
	AActor * obst1 = GetWorld()->SpawnActor<AActor>(obstacle, location, rotation);
	X_Offsets.Add(0);
	Y_Offsets.Add(offset);
	obstacles.Add(obst1);

	loc = location;
	loc.X += distance;
	X_Offsets.Add(distance);
	Y_Offsets.Add(offset);
	AActor * obst5 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst5);
}

void AFloor::SpawnInDiagonal() {
	int distance = FMath::RandRange(200, 350);
	int ndistance = distance *= -1;
	FVector location = this->GetActorLocation();
	FVector loc;
	FRotator rotation = this->GetActorRotation();
	AActor * obst1 = GetWorld()->SpawnActor<AActor>(obstacle, location, rotation);
	X_Offsets.Add(0);
	Y_Offsets.Add(0);
	obstacles.Add(obst1);

	loc = location;
	loc.X += distance;
	loc.Y += distance;
	X_Offsets.Add(distance);
	Y_Offsets.Add(distance);
	AActor * obst5 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst5);

}

void AFloor::SpawnOffCenter() {
	int offset = FMath::RandRange(-300, 350);
	int distance = FMath::RandRange(200, 300);
	int ndistance = distance *= -1;
	FVector loc;
	FVector location = this->GetActorLocation();
	FRotator rotation = this->GetActorRotation();
	AActor * obst1 = GetWorld()->SpawnActor<AActor>(obstacle, location, rotation);
	X_Offsets.Add(0);
	Y_Offsets.Add(offset);
	obstacles.Add(obst1);

	loc = location;
	loc.X += distance;
	X_Offsets.Add(distance);
	Y_Offsets.Add(offset);
	AActor * obst5 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst5);
}

void AFloor::SpawnInGrid() {
	int distance = FMath::RandRange(200, 300);
	int ndistance = distance *= -1;
	FVector loc;
	FVector location = this->GetActorLocation();
	FRotator rotation = this->GetActorRotation();
	AActor * obst1 =  GetWorld()->SpawnActor<AActor>(obstacle, location, rotation);
	X_Offsets.Add(0);
	Y_Offsets.Add(0);
	obstacles.Add(obst1);


	loc = location;
	loc.X += distance;
	loc.Y += ndistance;
	X_Offsets.Add(distance);
	Y_Offsets.Add(ndistance);
	AActor * obst2 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst2);

	loc = location;
	loc.X += ndistance;
	loc.Y += distance;
	X_Offsets.Add(ndistance);
	Y_Offsets.Add(distance);
	AActor * obst3 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst3);

	loc = location;
	loc.X += distance;
	loc.Y += distance;
	X_Offsets.Add(distance);
	Y_Offsets.Add(distance);
	AActor * obst5 = GetWorld()->SpawnActor<AActor>(obstacle, loc, rotation);
	obstacles.Add(obst5);



}
void AFloor::SpawnBone()
{
	if (CanSpawnBone) {
		FVector location = this->GetActorLocation();
		FRotator rotation = this->GetActorRotation();
		if (spawnBone != nullptr) {
			bone = GetWorld()->SpawnActor<AActor>(spawnBone, location, rotation);
			bone->SetActorLocation(location);
		}
	}
}

// Called every frame
void AFloor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector loc = this->GetActorLocation();
	if (Cast<AFloorParent>(owner))
		speed = Cast<AFloorParent>(owner)->GetSpeedLevel() * speedBase;
	SetActorLocation(GetActorLocation() - FVector(speed * DeltaTime, 0.0f, 0.0f));
;
	if (ball) {
		FVector ball_location = loc;
		ball_location.Z += Z_Offset_Ball;
		ball_location.Y += Y_Offset_Ball;
		ball->SetActorLocation(ball_location - FVector(speed * DeltaTime, 0.0f, 0.0f));
	}
	if (table) {
		FVector table_location = loc;
		table_location.Y += Y_Offset_Table;
		table->SetActorLocation(table_location - FVector(speed * DeltaTime, 0.0f, 0.0f));
	}
	if (bone) {
		FVector bone_location = loc;
		bone_location.Z += Z_Offset_Bone;
		bone_location.Y += Y_Offset_Bone;
		bone->SetActorLocation(bone_location - FVector(speed * DeltaTime, 0.0f, 0.0f));
	}

	FVector tree_location = this->GetActorLocation();
	for (int i = 0; i < trees.Num(); i++) {
		if (trees[i]) {
			tree_location.Y += Y_Offsets_Trees[i];
			trees[i]->SetActorLocation(tree_location - FVector(speed * DeltaTime, 0.0f, 0.0f));
		}
	}

	FVector obstacle_location = this->GetActorLocation();
	for (int i = 0; i < obstacles.Num(); i++) {
		if (obstacles[i]) {
			obstacle_location.Y += Y_Offsets[i];
			if (obstacle_location.Y < -312) {
				int new_offset = -312 + FMath::RandRange(0, 50);
				obstacle_location.Y = new_offset;
				Y_Offsets[i] = loc.Y - new_offset;
			}
			if (obstacle_location.Y > 348) {
				int new_offset = 348 - FMath::RandRange(0, 50);
				obstacle_location.Y = new_offset;
				Y_Offsets[i] = loc.Y - new_offset;
			}
			obstacle_location.X += X_Offsets[i];
			obstacle_location.Z = 50;
			obstacles[i]->SetActorLocation(obstacle_location - FVector(speed * DeltaTime, 0.0f, 0.0f));
		}
	
	}
  
	if (GetActorLocation().X < -2000.f)
	{
		if (ball) {
			ball->Destroy();
		}
		if (bone) {
			bone->Destroy();
		}
		if (table) {
			table->Destroy();
		}
		for (int i = 0; i < obstacles.Num(); i++) {
			if (obstacles[i]) {
				obstacles[i]->Destroy();
			}
		}
		Destroy();
		if (Cast<AFloorParent>(owner))
			Cast<AFloorParent>(owner)->CreateNewFloor();
	}
}

