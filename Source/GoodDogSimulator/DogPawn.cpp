// Fill out your copyright notice in the Description page of Project Settings.

#include "GoodDogSimulator.h"
#include "DogPawn.h"
#include "Floor.h"

// Sets default values
ADogPawn::ADogPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorEnableCollision(true);
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	OurVisibleComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("OurVisibleComponent"));
	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleCollider"));
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));

	IsJumping = false;
	FGravity = 10.0f;
	FJumpHeight = 200.0f;
    
    points = 0;
	lives = 3;
}

// Called when the game starts or when spawned
void ADogPawn::BeginPlay()
{
    if(CapsuleComponent)
    {
        CapsuleComponent->OnComponentBeginOverlap.AddDynamic(this, &ADogPawn::BeginOverlap);
    }
    Super::BeginPlay();
	FOriginalHeight = GetActorLocation().Z;
	OriginalLocation = GetActorLocation();

	DisableCollision();
	FTimerHandle timer2;
	GetWorld()->GetTimerManager().SetTimer(timer2, this, &ADogPawn::EnableCollision, 2.0f, false);
	Flicker();
}

// Called every frame
void ADogPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!CurrentVelocity.IsZero())
	{
		FVector NewLocation = GetActorLocation() + (CurrentVelocity * DeltaTime);
		SetActorLocation(NewLocation);
	}
	if (IsJumping)
	{
		ProcessJump();
	}
}

// Called to bind functionality to input
void ADogPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MoveRight", this, &ADogPawn::MoveRight);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ADogPawn::StartJump);
}

void ADogPawn::MoveForward(float AxisValue)
{
	// Move at 100 units per second forward or backward
	CurrentVelocity.X = FMath::Clamp(AxisValue, -1.0f, 1.0f) * 100.0f;
}

void ADogPawn::MoveRight(float AxisValue)
{
	// Move at 100 units per second right or left
	CurrentVelocity.Y = FMath::Clamp(AxisValue, -1.0f, 1.0f) * 200.0f;
}

UAudioComponent* ADogPawn::PlaySound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
	}
	return AC;
}

void ADogPawn::StartJump()
{
	if (IsJumping)
	{
		return;
	}
	IsJumping = true;
	CurrentVelocity.Z = FJumpHeight;
}

void ADogPawn::ProcessJump()
{
	if (FOriginalHeight < GetActorLocation().Z)
	{
		CurrentVelocity.Z -= FGravity;
	}
	else {
		IsJumping = false;
		CurrentVelocity.Z = 0.0f;

		//covers rare edge case where dog doesn't jump 
		FVector VActorLocation = GetActorLocation();
		VActorLocation.Z = FOriginalHeight;
		SetActorLocation(VActorLocation);
	}
}
void ADogPawn::BeginOverlap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult )
{
    // overlap code here
	if (OtherActor->IsA(AFloor::StaticClass()))
	{
		//ignore the overlap
	}
    else if(OtherActor != this)
    {
		FString string = OtherActor->GetName();
        //if it's a bone, add points, and make the bone dissapear
		if (string.Find("Obstacle") != -1) {
			lives--;
			if (lives == 0)
			{
				//process death
			}
            //make disappear for a bit, then put back in center
            this->SetActorHiddenInGame(true);
			DisableCollision();
            FTimerHandle timer1;
			FTimerHandle timer2;
            GetWorld()->GetTimerManager().SetTimer(timer1, this, &ADogPawn::Relocate, 1.0f, false);
			GetWorld()->GetTimerManager().SetTimer(timer2, this, &ADogPawn::EnableCollision, 2.0f, false);
		}
		else if (string.Find("BP_DogTreat") != -1) {
            OtherActor->SetActorHiddenInGame(true);
            points += 10;
		}
		FString PointText = "Your score: ";
		PointText.AppendInt(points);
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, PointText);
    }
}


void ADogPawn::Relocate()
{
	//put the dog back in the center
	SetActorLocation(OriginalLocation);
    this->SetActorHiddenInGame(false);
	Flicker();
}

void ADogPawn::Flicker()
{
	for (float i = 0.0f ; i < 1.0f; i += 0.15f)
	{
		FTimerHandle timer1;
		FTimerHandle timer2;
		GetWorld()->GetTimerManager().SetTimer(timer1, this, &ADogPawn::MakeInvisible, 0.1f + i, false);
		GetWorld()->GetTimerManager().SetTimer(timer2, this, &ADogPawn::MakeVisible, 0.2f + i, false);
	}

}

void ADogPawn::EnableCollision() 
{
	this->SetActorEnableCollision(true);
}

void ADogPawn::DisableCollision()
{
	this->SetActorEnableCollision(false);
}

void ADogPawn::MakeVisible()
{
	this->SetActorHiddenInGame(false);
}

void ADogPawn::MakeInvisible()
{
	this->SetActorHiddenInGame(true);
}




