// Fill out your copyright notice in the Description page of Project Settings.

#include "GoodDogSimulator.h"
#include "FloorParent.h"
#include "Floor.h"
#include "Components/ArrowComponent.h"

// Sets default values
AFloorParent::AFloorParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TempFloor = nullptr;
	floorsSpawned = 0;
	obstacles = 1;
	interval = 30;
	speedUpInt = 5.0f;
}

// Called when the game starts or when spawned
void AFloorParent::BeginPlay()
{
	Super::BeginPlay();
	if (FloorClass)
	{
		for (int i = 0; i < 19; i++)
			CreateNewFloor();
	}
	spawn = false;
	FTimerHandle LevelTimer;
	GetWorldTimerManager().SetTimer(LevelTimer, this, &AFloorParent::NextLevel, speedUpInt, true);
	//FTimerHandle timer;
	//GetWorldTimerManager().SetTimer(timer, &AFloorParent::IncreaseObstacles, interval, true);
}

int AFloorParent::GetConfiguration() {
	if (floorsSpawned >= 250) {
		return 7;
	}
	if (floorsSpawned >= 150) {
		return 6;
	}
	if (floorsSpawned >= 100) {
		return 5;
	}
	if (floorsSpawned >= 75) {
		return 4;
	}
	if (floorsSpawned >= 15) {
		return 3;
	}
	return 2;
}

void AFloorParent::NextLevel()
{
	speedLevel += 0.1f;
	speedUpInt += 10.0f;
	FString debug = "SpeedLevel: " + FString::SanitizeFloat(speedLevel);
	if (GEngine)
	{ }
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, debug);
}

// Called every frame
void AFloorParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFloorParent::CreateNewFloor()
{
	UWorld* World = GetWorld();
	if (World)
	{
		// Set Spawn Parameters
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = Instigator;
		// If there are not Floor in the world, create one
		if (FloorArray.Num() <= 0)
		{
			TempFloor = World->SpawnActor<AFloor>(FloorClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		}
		// If there is at least one Floor, create a new one and attach to the last Floor in TArray
		else
		{
			UArrowComponent* AttachPoint = FloorArray[FloorArray.Num()-1]->FindComponentByClass<UArrowComponent>();
			TempFloor = World->SpawnActor<AFloor>(FloorClass, AttachPoint->GetComponentLocation(), FRotator::ZeroRotator, SpawnParams);
			//TempFloor->SpawnItem();
		}
		TempFloor->SetOwner(this);
		/*if (GEngine&&TempFloor->GetOwner())
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TempFloor->GetOwner()->GetName());*/
		if (TempFloor)
			FloorArray.Add(TempFloor);
	}
	floorsSpawned++;
}

