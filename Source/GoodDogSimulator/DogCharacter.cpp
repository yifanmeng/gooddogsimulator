// Fill out your copyright notice in the Description page of Project Settings.

#include "GoodDogSimulator.h"
#include "DogCharacter.h"
#include "Floor.h"
#include "Blueprint/UserWidget.h"

// Sets default values
ADogCharacter::ADogCharacter()
{
	// Set this Character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetActorEnableCollision(true);
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	OurVisibleComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("OurVisibleComponent"));
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));

	IsJumping = false;
	FGravity = 700.0f;
	FJumpHeight = 450.0f;
	FMoveSpeed = 250.0f;
	FSpeedUpTime = 5.0f;
	FAnimationSpeed = 1.5f;

	points = 0;
	lives = 3;
}

// Called when the game starts or when sCharactered
void ADogCharacter::BeginPlay()
{

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ADogCharacter::BeginOverlap);
	Super::BeginPlay();
	FOriginalHeight = GetActorLocation().Z;
	OriginalLocation = GetActorLocation();

	DisableCollision();
	FTimerHandle timer2;
	GetWorld()->GetTimerManager().SetTimer(timer2, this, &ADogCharacter::EnableCollision, 2.0f, false);
	Flicker();
	PlaySound(BackgroundCue);

	FTimerHandle timer1;
	GetWorldTimerManager().SetTimer(timer1, this, &ADogCharacter::SpeedUp, FSpeedUpTime, true);
}

// Called every frame
void ADogCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!CurrentVelocity.IsZero())
	{
		FVector NewLocation = GetActorLocation() + (CurrentVelocity * DeltaTime);
		SetActorLocation(NewLocation);
	}
	if (IsJumping)
	{
		ProcessJump(DeltaTime);
	}
}

void ADogCharacter::SpeedUp()
{
	//FAnimationSpeed += 0.1f;
	FMoveSpeed += 15.0f;
	//GetMesh()->SetPlayRate(FAnimationSpeed);
}

// Called to bind functionality to input
void ADogCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MoveRight", this, &ADogCharacter::MoveRight);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ADogCharacter::StartJump);
	InputComponent->BindAction("Bark", IE_Pressed, this, &ADogCharacter::Bark);
	InputComponent->BindAction("Slide", IE_Pressed, this, &ADogCharacter::Slide);
}

void ADogCharacter::MoveForward(float AxisValue)
{
	// Move at 100 units per second forward or backward
	CurrentVelocity.X = FMath::Clamp(AxisValue, -1.0f, 1.0f) * 100.0f;
}

void ADogCharacter::MoveRight(float AxisValue)
{
	// Move at 100 units per second right or left

	FVector VActorLocation = GetActorLocation();
	if (VActorLocation.Y>=380)
		CurrentVelocity.Y = FMath::Clamp(AxisValue, -1.0f, 0.0f) * FMoveSpeed;
	else if (VActorLocation.Y <= -380)
		CurrentVelocity.Y = FMath::Clamp(AxisValue, 0.0f, 1.0f) * FMoveSpeed;
	else
		CurrentVelocity.Y = FMath::Clamp(AxisValue, -1.0f, 1.0f) * FMoveSpeed;

}

UAudioComponent* ADogCharacter::PlaySound(USoundCue* Sound)
{
	UAudioComponent* AC = NULL;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, RootComponent);
	}
	return AC;
}

void ADogCharacter::StartJump()
{
	if (IsJumping)
	{
		return;
	}
	IsJumping = true;
	CurrentVelocity.Z = FJumpHeight;
	GetMesh()->PlayAnimation(JumpAnim, false);
}

void ADogCharacter::ProcessJump(float DeltaTime)
{
	if (FOriginalHeight < GetActorLocation().Z)
	{
		CurrentVelocity.Z -= FGravity * DeltaTime;
		/*if (GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 0.2f, FColor::Yellow, FString::SanitizeFloat(CurrentVelocity.Z));*/
	}
	else {
		IsJumping = false;
		CurrentVelocity.Z = 0.0f;
		GetMesh()->PlayAnimation(RunAnim, true);

		//covers rare edge case where dog doesn't jump 
		FVector VActorLocation = GetActorLocation();
		VActorLocation.Z = FOriginalHeight;
		SetActorLocation(VActorLocation);
	}
}
void ADogCharacter::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	// overlap code here
	if (OtherActor->IsA(AFloor::StaticClass()))
	{
		//ignore the overlap
	}
	else if (OtherActor != this)
	{
		FString string = OtherActor->GetName();
		//if it's a bone, add points, and make the bone dissapear
		if (string.Find("BP_Rock") != -1 || string.Find("BP_Table") != -1) {
			PlaySound(ObstacleCue);
			lives--;
			if (lives == 0)
			{
				//process death
				DisableCollision();
				if (wSubmit)
				{
					SubmitWidget = CreateWidget<UUserWidget>(GetWorld(), wSubmit);
					if (SubmitWidget)
					{
						SubmitWidget->AddToViewport();
						APlayerController* MyController = GetWorld()->GetFirstPlayerController();

						MyController->bShowMouseCursor = true;
						MyController->bEnableClickEvents = true;
						MyController->bEnableMouseOverEvents = true;

					}
				}
				return;
				//UGameplayStatics::OpenLevel(GetWorld(), "MainMenu");
			}
            //make disappear for a bit, then put back in center
			this->SetActorHiddenInGame(true);
			DisableCollision();
			FTimerHandle timer1;
			FTimerHandle timer2;
			GetWorld()->GetTimerManager().SetTimer(timer1, this, &ADogCharacter::Relocate, 1.0f, false);
			GetWorld()->GetTimerManager().SetTimer(timer2, this, &ADogCharacter::EnableCollision, 2.0f, false);
		}
		else if (string.Find("BP_DogTreat") != -1) {
			OtherActor->SetActorHiddenInGame(true);
			points += 10;
            PlaySound(CollectCue);
		}
		else if (string.Find("BP_Ball") != -1) {
			OtherActor->SetActorHiddenInGame(true);
			points += 50;
			PlaySound(CollectCue);
		}
		FString PointText = "Your score: ";
		PointText.AppendInt(points);
		//GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Yellow, PointText);
	}
}


void ADogCharacter::Relocate()
{
	//put the dog back in the center
	SetActorLocation(OriginalLocation);
	this->SetActorHiddenInGame(false);
	Flicker();
	ResumeRun();
}

void ADogCharacter::Flicker()
{
	for (float i = 0.0f; i < 0.8f; i += 0.15f)
	{
		FTimerHandle timer1;
		FTimerHandle timer2;
		GetWorld()->GetTimerManager().SetTimer(timer1, this, &ADogCharacter::MakeInvisible, 0.1f + i, false);
		GetWorld()->GetTimerManager().SetTimer(timer2, this, &ADogCharacter::MakeVisible, 0.2f + i, false);
	}

}

void ADogCharacter::Bark()
{
    PlaySound(BarkCue);
}

void ADogCharacter::Slide()
{
	GetMesh()->PlayAnimation(SlideAnim, false);
	FTimerHandle handle;
	GetWorld()->GetTimerManager().SetTimer(handle, this, &ADogCharacter::ResumeRun, 0.85f);
}

void ADogCharacter::ResumeRun()
{
	GetMesh()->PlayAnimation(RunAnim, true);
	//GetMesh()->SetPlayRate(FAnimationSpeed);
}

void ADogCharacter::EnableCollision()
{
	this->SetActorEnableCollision(true);
}

void ADogCharacter::DisableCollision()
{
	this->SetActorEnableCollision(false);
}

void ADogCharacter::MakeVisible()
{
	this->SetActorHiddenInGame(false);
}

void ADogCharacter::MakeInvisible()
{
	this->SetActorHiddenInGame(true);
}




