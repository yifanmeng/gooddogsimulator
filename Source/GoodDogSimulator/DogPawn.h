// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "DogPawn.generated.h"

UCLASS()
class GOODDOGSIMULATOR_API ADogPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ADogPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;	
	UPROPERTY(EditAnywhere)
		USceneComponent* OurVisibleComponent;
    UPROPERTY(EditAnywhere, Category="Collision")
		UCapsuleComponent* CapsuleComponent;
	UPROPERTY(EditAnywhere, Category = "Sound")
		UAudioComponent* AudioComponent;
	UPROPERTY(EditAnywhere, Category = "Sound")
		USoundCue* BarkCue;
	UPROPERTY(EditAnywhere, Category = "Movement")
		float FGravity;
	UPROPERTY(EditAnywhere, Category = "Movement")
		float FJumpHeight;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		class UAnimMontage* JumpAnim;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		class UAnimMontage* RunAnim;

	UFUNCTION()
    void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);

	UAudioComponent* PlaySound(USoundCue* Sound);

	void StartJump();
	void ProcessJump();

	void Relocate();
	void EnableCollision();
	void DisableCollision();
	
	void Flicker();
	void MakeVisible();
	void MakeInvisible();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int points;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int lives;

private:
	bool IsJumping;
	float FOriginalHeight;
	FVector OriginalLocation;
	FVector CurrentVelocity;
};
