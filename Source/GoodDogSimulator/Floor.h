// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Floor.generated.h"


UCLASS()
class GOODDOGSIMULATOR_API AFloor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFloor();
//	AFloor(int);
	UBoxComponent * spawnArea;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	float speed;
	float speedBase = 1000.0f;
	AActor * owner;

public:	
	virtual void Tick(float DeltaTime) override;
	void SetOwner(AActor* val) { owner = val; }
	void SpawnBone();
	void SpawnObstacle();
	void SpawnBall();
	void SpawnTable();
	void SpawnTrees();
	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> spawnBone;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> spawnBall;
    UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> obstacle;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> spawnTable;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> spawnTree;
	UPROPERTY(EditAnywhere)
		TSubclassOf<AActor> spawnShrub;
	UPROPERTY(EditAnywhere)
		USceneComponent* SceneComponent;

	void SpawnInVerticalLineOffset();
	void SpawnInHorizontalLineOffset();
	void SpawnInHorizontalLine();
	void SpawnInVerticalLine();
	void SpawnInDiagonal();
	void SpawnInGrid();
	void SpawnOffCenter();
	AActor* bone;
	AActor * ball;
	AActor* table;
	TArray<AActor*> trees;
	TArray<AActor*> shrubs;
	TArray<AActor*> obstacles;
	TArray<int> Y_Offsets;
	TArray<int> Z_Offsets;
	TArray<int> X_Offsets;
	TArray<int> Y_Offsets_Trees;
	TArray<int> Y_Offsets_Shrubs;
	float Y_Offset_Ball;
	float Z_Offset_Ball;
	float Y_Offset_Bone;
	float Z_Offset_Bone;
	float Y_Offset_Table;
	int config = 5;
	bool CanSpawnBone = true;
};
