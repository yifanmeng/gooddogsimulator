// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "GameFramework/SaveGame.h"
#include "DoggoSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class GOODDOGSIMULATOR_API UDoggoSaveGame : public USaveGame
{
	GENERATED_BODY()	
public:
	UDoggoSaveGame();
	UFUNCTION(BlueprintCallable)
		void AddScore(FString name, FString score);
	UFUNCTION(BlueprintCallable)
		FString GetScores();
private:
	void SortScores();
	TArray<FString> names;
	TArray<int> scores;
};
