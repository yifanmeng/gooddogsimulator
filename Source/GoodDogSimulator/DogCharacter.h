// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "DogCharacter.generated.h"

UCLASS()
class GOODDOGSIMULATOR_API ADogCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADogCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
		USceneComponent* OurVisibleComponent;
	UPROPERTY(EditAnywhere, Category = "Sound")
		UAudioComponent* AudioComponent;
	UPROPERTY(EditAnywhere, Category = "Sound")
		USoundCue* BarkCue;
    UPROPERTY(EditAnywhere, Category = "Sound")
        USoundCue* CollectCue;
    UPROPERTY(EditAnywhere, Category = "Sound")
        USoundCue* ObstacleCue;
	UPROPERTY(EditAnywhere, Category = "Sound")
		USoundCue* BackgroundCue;
	UPROPERTY(EditAnywhere, Category = "Movement")
		float FGravity;
	UPROPERTY(EditAnywhere, Category = "Movement")
		float FJumpHeight;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
		class UAnimSequence* JumpAnim;
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		class UAnimSequence* RunAnim;
	UPROPERTY(EditDefaultsOnly, Category = Animation)
		class UAnimSequence* SlideAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> wSubmit;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		UUserWidget* SubmitWidget;

	UFUNCTION()
		void BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);
	
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);

	UAudioComponent* PlaySound(USoundCue* Sound);

	void StartJump();
	void ProcessJump(float DeltaTime);
	void Slide();
	void ResumeRun();
	void SpeedUp();

	void Relocate();
	void EnableCollision();
	void DisableCollision();

	void Flicker();
	void MakeVisible();
	void MakeInvisible();
    
    void Bark();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int points;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int lives;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FMoveSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FSpeedUpTime;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float FAnimationSpeed;

private:
	bool IsJumping;
	float FOriginalHeight;
	FVector OriginalLocation;
	FVector CurrentVelocity;
};
